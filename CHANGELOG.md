# Changelog

Minds Mobile

## 4.17.0 - 2021-08-27

### Changed

- Performance improvements
- New and faster storage system
- Keeps awake device while playing a video
- Improved UI/UX in discovery
- New interaction modals design
- Bug fixing

## 4.16.0 - 2021-07-26

### Changed

- Fix comments scroll problems
- Date format
- Detect chat app installed on iOS
- Improves UX on comments
- Bug fixing

## 4.15.0 - 2021-07-10

### Changed

- New bottom sheets
- New password reset screens
- Keyboard dark theme support on iOS
- Snowplow analytics
- Search filter added to discovery
- Bug fixing

## 4.14.3 - 2021-07-01

### Changed

- Fix notifications navigation

## 4.14.2 - 2021-06-25

### Changed

- Fix push notification settings

## 4.14.1 - 2021-06-22

### Changed

- Fix 2FA

## 4.14.0 - 2021-06-21

### Changed

- New notifications
- Interaction details
- Translation support added to comments
- Bug fixing

## 4.13.0 - 2021-05-21

### Changed

- Performance improvements
- Media attachment can now be edited on comments
- Allow exporting legacy wallets more than once
- Scroll added to the main menu for small screens
- Bug fixing

## 4.12.0 - 2021-05-05

### Changed

- Images pre-load on feeds
- New chat integration
- Improved deeplink support
- New sessions screen
- Bug fixing

## 4.11.1 - 2021-04-23

### Changed

- Bug fixing

## 4.11.0 - 2021-03-25

### Changed

- Channel screen styling improvements
- Updated react-native to 0.64
- Bug fixing

## 4.10.0 - 2021-03-20

### Changed

- Two-Factor Auth
- Bug fixing
